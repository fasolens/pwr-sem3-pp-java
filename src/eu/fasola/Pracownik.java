package eu.fasola;

import java.util.Random;

public class Pracownik {
    protected String nazwisko;
    protected String imie;
    protected double placa;

    Pracownik (String imie, String nazwisko) {
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.placa = 0;
    }
    Pracownik (String imie, String nazwisko, double placa) {
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.placa = placa;
    }

    protected void podwyzka(double ile) {
        this.placa = this.placa + ile*this.placa;
    }

    @Override
    public String toString() {
        return nazwisko + " " + imie + " " + placa;
    }
}
