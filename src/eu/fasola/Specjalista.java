package eu.fasola;

public class Specjalista extends Pracownik {
    private double premia;
    protected void podwyzkaPremii(double ile) {
        this.premia = this.premia + ile;
    }

    Specjalista(String imie, String nazwisko, double placa, double premia) {
        super(imie, nazwisko, placa);
        this.premia = premia;
    }

    @Override
    public String toString() {
        return super.toString() + " " + premia;
    }
}
