package eu.fasola;

import java.util.ArrayList;

public class Firma {
    private ArrayList<Pracownik> listaPracownikow;
    protected String nazwa;
    Firma () {
        this.nazwa = "Firma Bez Nazwy";
        listaPracownikow = new ArrayList<Pracownik>();
    }
    Firma (String nazwa) {
        this.nazwa = nazwa;
        listaPracownikow = new ArrayList<Pracownik>();
    }
    protected void dodajPracownika(String imie, String nazwisko) {
        listaPracownikow.add(new Pracownik(imie, nazwisko));
    }
    protected void dodajPracownika(String imie, String nazwisko, double placa) {
        listaPracownikow.add(new Pracownik(imie, nazwisko, placa));
    }
    protected void dodajPracownika(String imie, String nazwisko, double placa, double premia) {
        listaPracownikow.add(new Specjalista(imie, nazwisko, placa, premia));
    }
    protected void dodajPracownika(Pracownik pracownik) {
        listaPracownikow.add(pracownik);
    }
    protected void dajPodwyzkeDlaWszystkich(double premiaProc) {
        for (Pracownik p : listaPracownikow) {
            p.podwyzka(premiaProc/100);
        }
    }
    protected void podniesPremieSpecjalista(double premiaProc) {
        for (Pracownik p : listaPracownikow) {
            if (p instanceof Specjalista) {
                ((Specjalista)p).podwyzkaPremii(premiaProc);
            }
        }
    }

    @Override
    public String toString() {
        String r = this.nazwa + "\n";
        for (Pracownik p : listaPracownikow) {
            r += p.toString() + "\n";
        }
        return r;
    }
}
