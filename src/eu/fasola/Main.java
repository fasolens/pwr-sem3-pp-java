package eu.fasola;

//(10) [Java] Stworzyć klasę Pracownik posiadająca pola
// nazwisko,
// imię,
// płaca.
//        Dodatkowo istnieje pracownik
// Specjalista, który ma dodatkowo pole
// premia.
//        Każdemu z pracowników można dać podwyżkę
// procentową, specjaliście oprócz tego
//        można
// podwyższyć premię.
// Każda z klas ma mieć metodę
// toString() wypisującą
//        informację o obiekcie.
//        Stworzyć klasę Firma do pamiętania pracowników (na prostej tablicy lub kolekcji).
// W
//        firmie istniej możliwość
// dodania nowego pracownika,
// podwyższenia wszystkim pracownikom płacy o ustalony procent.

// Dodatkowo istnieje możliwość podwyższenia
//        premii specjalistom o ustaloną wartość.
// Metoda toString() prezentuje zawartość klasy
//        Firma.
//        Napisać program testujący działanie wszystkich klas.

public class Main {

    public static void main(String[] args) {
        Firma firma = new Firma("IoT Company Inc.");
        firma.dodajPracownika("Cezary", "Kowalski");
        firma.dodajPracownika("Wojciech", "Kowalski", 2800);
        firma.dodajPracownika("Stefan", "Nowak", 2345.76, 1000);
        System.out.println(firma);
        System.out.println("Podwyzka o 50%");
        firma.dajPodwyzkeDlaWszystkich(50);
        System.out.println(firma);
        firma.podniesPremieSpecjalista(25);
        System.out.println(firma);
    }
}
